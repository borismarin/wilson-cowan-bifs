#!/usr/bin/python
import auto
auto.clean()
n = auto.load('wilson_cowan')
print('integrando')
init = auto.run(n, c='integra')
auto.plot(init, bifurcation_x=['PAR(14)'], bifurcation_y=['E'], use_labels=0)
print('continuando equilíbrio')
eqs = auto.run(init()[-1], c='eq')
eqs = auto.run(eqs, DS='-')

print('continuando ciclos que nascem na Hopf')
ciclos = auto.run(eqs('HB1'), c='tubo', STOP=['BP1'])
auto.plot(eqs+ciclos, use_labels=False, bifurcation_x=['P'],
        bifurcation_y=['MAX E', 'MIN E'], stability=True)


print('continuando sela-nó em 2 parâmetros')
fold = auto.load(eqs('LP1'), c='fold')
folds = auto.run(fold)
folds += auto.run(fold, DS='-')
folds = auto.merge(folds)

print('continuando Hopf em 2 parâmetros')
hopf = auto.load(eqs('HB1'), c='hopf')
hopfs = auto.run(hopf)
hopfs += auto.run(hopf, DS='-')
hopfs = auto.merge(hopfs)


auto.plot(folds+hopfs, bifurcation_x=['P'], bifurcation_y=['Q'], stability=False, use_labels=False)


#include "auto_f2c.h"
/* ---------------------------------------------------------- */
/*  Wilson - Cowan usando notação do paper de 1972            */
/* ---------------------------------------------------------- */

double sigmoid(double x, double a, double theta){
    return 1./(1.+exp(-a*(x-theta))) - 1./(1.+exp(a*theta));
}

int func (integer ndim, const doublereal *U, const integer *icp,
          const doublereal *par, integer ijac,
          doublereal *F, doublereal *dfdu, doublereal *dfdp)
{

    double P = par[0];
    double Q = par[1];
    double wEE = par[2];
    double wEI = par[3];
    double wIE = par[4];
    double wII = par[5];

    const double aE = 1.3;
    const double thetaE = 4;
    const double aI = 2;
    const double thetaI = 3.7;
    const double rE = 1.;
    const double rI = 1.;
    
    double E = U[0];
    double I = U[1];

    F[0] = -E + (1-rE*E) * sigmoid(wEE*E - wEI*I + P, aE, thetaE);
    F[1] = -I + (1-rI*I) * sigmoid(wIE*E - wII*I + Q, aI, thetaI);

  return 0;
}
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
int stpnt (integer ndim, doublereal t,
           doublereal *u, doublereal *par)
{

  par[0] = 0.5; // P
  par[1] = 0.;  // Q
  par[2] = 16.; // wEE
  par[3] = 12.; // wEI
  par[4] = 15.; // wIE
  par[5] = 3.;  // wII

  u[0] = 0.1;
  u[1] = 0.1;

  return 0;
}
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
int pvls (integer ndim, const doublereal *u,
          doublereal *par)
{
  par[6] = getp("MIN", 1, u); //mínimo de E
  par[7] = getp("MAX", 1, u); //máximo de E
  par[8] = getp("MIN", 2, u); //mínimo de I
  par[9] = getp("MAX", 2, u); //máximo de I
  return 0;
}
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
int bcnd (integer ndim, const doublereal *par, const integer *icp,
          integer nbc, const doublereal *u0, const doublereal *u1, integer ijac,
          doublereal *fb, doublereal *dbc)
{
  return 0;
}
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
int icnd (integer ndim, const doublereal *par, const integer *icp,
          integer nint, const doublereal *u, const doublereal *uold,
          const doublereal *udot, const doublereal *upold, integer ijac,
          doublereal *fi, doublereal *dint)
{
  return 0;
}
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
int fopt (integer ndim, const doublereal *u, const integer *icp,
          const doublereal *par, integer ijac,
          doublereal *fs, doublereal *dfdu, doublereal *dfdp)
{
  return 0;
}
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */


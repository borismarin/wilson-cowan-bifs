parnames = {1:'mu'}
unames = {1:'x', 2:'y'}

NDIM = 2, IPS = 1, IRS = 0, ILP = 1
ICP = ['mu']

NTST = 50, NCOL = 4, IAD = 3, ISP =  2, ISW = 1, IPLT = 1
NMX = 10000, NPR = 100000, IID = 2, ITMX = 10, ITNW = 7, NWTN = 3

EPSL = 1e-8, EPSU = 1e-8, EPSS = 1e-5
DS = 0.01, DSMIN = 1e-10, DSMAX = 0.01, IADS = 1

UZSTOP = {'mu':[-1,1]}

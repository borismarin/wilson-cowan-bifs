#!/usr/bin/python
import auto
auto.clean()
n = auto.load('transcritica')
print('continuando equilíbrio')
eqs = auto.run(c='eq')
#eqs += auto.run(eqs, DS='-')

auto.plot(eqs, bifurcation_x=['mu'], bifurcation_y=['x'], stability=True, use_labels=False)
auto.plot(eqs, bifurcation_x=['mu'], bifurcation_y=['y'], stability=True, use_labels=False)


parnames = {1:'P', 2:'Q', 3:'wEE', 4:'wEI', 5:'wIE', 6:'wII', 7:'MIN E', 8:'MAX E', 9:'MIN I', 10:'MAX I'}
unames = {1:'E', 2:'I'}

NDIM = 2, IPS = 1, IRS = 0, ILP = 1
ICP = ['P', 'Q']

NTST = 50, NCOL = 4, IAD = 3, ISP =  2, ISW = 1, IPLT = 1
NMX = 10000, NPR = 100000, IID = 2, ITMX = 10, ITNW = 7, NWTN = 3

EPSL = 1e-8, EPSU = 1e-8, EPSS = 1e-5
DS = 0.01, DSMIN = 1e-10, DSMAX = 0.01, IADS = 1

NPAR = 11 

UZSTOP = {'P':[-5,5], 'Q':[-5,5]}
